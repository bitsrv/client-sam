package client.sam

import wslite.http.auth.HTTPBasicAuthorization
import wslite.soap.SOAPClient
import java.text.SimpleDateFormat

class SamClient {

    def soapClient

    SamClient(String url, String username, String password) {
        def clientBasicAuth = new HTTPBasicAuthorization()
        clientBasicAuth.username = username
        clientBasicAuth.password = password

        soapClient = new SOAPClient(url)
        soapClient.authorization = clientBasicAuth
    }

    def verifyPassword(String uId, String pass) {
        def response = soapClient.send(sslTrustAllCerts:true) {
            body {
                verifyPassword {
                    userId(uId)
                    password(pass)
                }
            }
        }

        if(response?.verifyPasswordResponse?.return?.errorCode == 0) {
            return true
        }

        return false
    }

    def queryUser(String uId) {
        def response = soapClient.send(sslTrustAllCerts:true) {
            body {
                queryUser {
                    params {
                        userId(uId)
                    }
                }
            }
        }

        return response?.queryUserResponse?.queryUserResult
    }

    def queryOnlineUser(String uId) {
        def response = soapClient.send(sslTrustAllCerts:true) {
            body {
                queryOnlineUser {
                    param {
                        userId(uId)
                    }
                }
            }
        }

        return response?.queryOnlineUserResponse?.return
    }

    def queryNtdFlow(String uId, int offset = 0, int lim = 10) {
        def sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX")

        // 当月第一天
        Calendar c = new GregorianCalendar()
        c.set(Calendar.DAY_OF_MONTH, 1)
        c.set(Calendar.HOUR_OF_DAY, 0)
        c.set(Calendar.MINUTE, 0)
        c.set(Calendar.SECOND, 0)
        c.set(Calendar.MILLISECOND, 0)

        def response = soapClient.send(sslTrustAllCerts:true) {
            body {
                queryNtdFlow {
                    params {
                        userId(uId)
                        fromDate(sdf.format(c.getTime()))
                        offSet(offset)
                        limit(lim)
                    }
                }
            }
        }

        return response?.queryNtdFlowResponse?.queryNtdFlowResult
    }
}

