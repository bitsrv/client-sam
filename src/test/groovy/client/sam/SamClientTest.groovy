package client.sam

import spock.lang.Specification
import java.text.SimpleDateFormat

class SamClientTest extends Specification {
    String url = 'https://localhost:8000/sam/services/samapi'
    String username = 'weixin'
    String password = '2016^WeiXin'

    def "用户验证"() {
        setup:
        def client = new SamClient(url, username, password)

        when:
        def r = client.verifyPassword('2015010097', 'liuzhw')

        then:
        r == true
    }

    def "查询用户"(){
        setup:
        def client = new SamClient(url, username, password)

        when:
        def r = client.queryUser('2015010097')

        then:
        r.errorCode == 0
    }

    def "查询在线用户"(){
        setup:
        def client = new SamClient(url, username, password)

        when:
        def r = client.queryOnlineUser('2015010097')

        then:
        r.errorCode == 0
    }

    def "查询流量"(){
        setup:
        def client = new SamClient(url, username, password)

        when:
        def r = client.queryNtdFlow('2015010097', 0, 10)

        then:
        r.errorCode == 0
    }
}
